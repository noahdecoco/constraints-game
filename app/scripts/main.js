(function (){

	'use strict';

	// VARIABLES

	var touched = false;
	var numCards = 3;
	var infoOpen = false;
	var pickedCards = [];
	var cardArray = [];

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 6000 );
	var renderer = new THREE.WebGLRenderer();
	var light = new THREE.PointLight( 0xffffff, 1, 10000 );

	// SETUP SCENE

	renderer.setSize( window.innerWidth, window.innerHeight );
	document.body.appendChild( renderer.domElement );
	camera.position.z = 700;
	light.position.set(0,0,1000);
	scene.add( light );

	function render() {
		requestAnimationFrame(render);
		cardArray.forEach(function(card, index){
			if((pickedCards.indexOf(index) > -1) === false) {
				card.rotation.x += 0.002;
				card.rotation.y += 0.002;
			}
		});
		renderer.render(scene, camera);
	}

	// SETUP CARDS

	for ( var i = 1; i <= 52; i ++ ) {
		var card = new THREE.BoxGeometry(400, 286, 20);

		var cardMaterialArray = [];
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { color: 0x000000 } ) );
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { color: 0x000000 } ) );
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { color: 0x000000 } ) );
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { color: 0x000000 } ) );
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture( 'images/card-'+i+'.jpg'), overdraw: true } ) );
		cardMaterialArray.push( new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture( 'images/card-back.jpg'), overdraw: true } ) );
		var cubeMaterials = new THREE.MeshFaceMaterial( cardMaterialArray );
		var cube = new THREE.Mesh( card, cubeMaterials );

		cardArray.push(cube);
		scene.add( cube );
	}

	function shuffleCards(){
		cardArray.forEach(function(card, index){
			TweenMax.to(card.position, 2+Math.random(), {
				x : Math.random()*10000 - 5000,
				y : Math.random()*10000 - 5000,
				z : (Math.random()*2000) - 3000,
				ease: Circ.easeOut
			});
			TweenMax.to(card.rotation, 1, {
				x : Math.random()*180*(Math.PI/180),
				y : Math.random()*180*(Math.PI/180),
				z : Math.random()*180*(Math.PI/180)
			});
		});
	}

	function pickCards(){
		pickedCards.length = 0;
		for ( var i = 1; i <= numCards; i ++ ) {
			var pick = Math.floor(Math.random()*52);
			while((pickedCards.indexOf(pick) > -1)) {
				pick = Math.floor(Math.random()*52);
			}
			pickedCards.push(pick);
		}
	}

	function showCards(){
		if (numCards === 1){
			pickedCards.forEach(function(card, index){
				TweenMax.to(cardArray[card].position, 1+Math.random(), {
					x : 0,
					y : 0,
					z : -100,
					ease: Circ.easeOut
				});
				TweenMax.to(cardArray[card].rotation, 1+Math.random(), {
					x : 0,
					y : 0,
					z : 0
				});
			});
		} else {
			pickedCards.forEach(function(card, index){
				TweenMax.to(cardArray[card].position, 1+Math.random(), {
					x : (index+1-2)*500,
					y : 0,
					z : -100,
					ease: Circ.easeOut
				});
				TweenMax.to(cardArray[card].rotation, 1+Math.random(), {
					x : 0,
					y : (10-((index+1)*5))*(Math.PI/180),
					z : 0
				});
			});
		}
	}
	
	// INTERACTIVITY

	$('canvas').click(function(){
		if(touched === false){
			touched = true;
			TweenMax.to($('#splash'),0.5,{autoAlpha:0});
			ga('send','event', 'Game', 'Start');
		}
		if(infoOpen){
			TweenMax.to($('#info'),0.5,{left:'-400px'});
			infoOpen = false;
		} else {
			ga('send','event', 'Game', 'Play');
			shuffleCards();
			pickCards();
			showCards();
		}
	});

	$('body').mousemove(function(e){
		TweenMax.to(camera.position, 0.5, {
			x : (window.innerWidth/2-e.offsetX),
			y : (window.innerHeight/2-e.offsetY)*-1,
			onUpdate: function(){
				camera.lookAt( scene.position );
			}
		});
	});

	$('.button')
		.mouseover(function(){
			TweenMax.to($(this), 0.5, {opacity:1});
		})
		.mouseout(function(){
			TweenMax.to($(this), 0.5, {opacity:0.5});
		});

	$('#info-btn').click(function(e){
		infoOpen = true;
		TweenMax.to($('#info'),0.5,{left:0});
	});

	$('#game-change-btn')
		.click(function(e){
			if (numCards === 3) {
				numCards = 1;
				ga('send','event', 'Game', 'Change mode', '1-card');
				$('#1-card').show();
				$('#3-card').hide();
			} else {
				numCards = 3;
				ga('send','event', 'Game', 'Change mode', '3-card');
				$('#1-card').hide();
				$('#3-card').show();
			}
			shuffleCards();
			pickCards();
			showCards();
		});

	// INITIALISE

	$('#1-card').hide();
	render();
	shuffleCards();

})();